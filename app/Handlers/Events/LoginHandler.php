<?php

namespace JPAPhotography\Handlers\Events;

use JPAPhotography\Utils\LogUtil;
use Illuminate\Support\Facades\Session;
use JPAPhotography\User;
use JPAPhotography\Models\Role;
use Illuminate\Support\Facades\DB;

class LoginHandler {

    public function handle(User $user) {
        LogUtil::enterMethod('LoginHandler::handle()');
        // dd($user);
        $pr = $this->getUserRolesAndPermissions($user);
        
        $userArray = array (
            'id' => $user->ID,
            'username' => $user->user_login,
            'name' => $user->name,
            'email' => $user->email,
            'permissions' => $pr['permissions'],
            'roles' => $pr['roles']
        );
        
        Session::set('user', $userArray);
        LogUtil::exitMethod('LoginHandler::handle()');
    }
    
    private function getUserRolesAndPermissions(User $user) {
        $assRoles = $user->assignedRoles;
        
        $userRoles = array();
        foreach($assRoles as $role) {
            $userRoles[] = $role->role_id;
        }
        
        $rolesTable = DB::table('roles')
                 ->join('assigned_roles', 'assigned_roles.role_id', '=', 'roles.id')
                 ->select('roles.name')
                 ->where('assigned_roles.user_id', '=', $user->ID)
                 ->get();
        
        $roles = array();
        foreach ($rolesTable as $role) {
            $roles[] = $role->name;
        }
        
        $permissionsTable = DB::table('permissions')
            ->join('permission_role', 'permissions.id', '=', 'permission_role.permission_id')
            ->select('permissions.name')
            ->whereIn('permission_role.role_id', $userRoles)
            ->get();
        
        $permissions = array();
        foreach ($permissionsTable as $permission) {
            $permissions[] = $permission->name;
        }
        
        $pr = array(
            'roles' => $roles,
            'permissions' => $permissions
        );
        
        return $pr;
    }
}