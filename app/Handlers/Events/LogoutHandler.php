<?php

namespace JPAPhotography\Handlers\Events;

use JPAPhotography\Utils\LogUtil;
use Illuminate\Support\Facades\Session;
class LogoutHandler {
    public function handle($user) {
        LogUtil::enterMethod('LogoutHandler::handle()');
        //dd($user);
        Session::forget('user');
        LogUtil::exitMethod('LogoutHandler::handle()');
    }
}