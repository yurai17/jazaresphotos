<?php

namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JPAPhotography\Http\Requests\ContactFormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Anhskohbo\NoCaptcha\Facades\NoCaptcha;

class AboutController extends Controller {

    public function getIndex(Request $req) {
        return view ('site/about/us');
    }

    public function getContactUs(Request $req) {
        Session::remove('success');
        return view ('site/about/contact');
    }
    
    public function getFaq(Request $req) {
        return view('site/about/faq');
    }

    public function postContactUs(ContactFormRequest $req) {
        $values = [
            'name' => $req->name,
            'email' => $req->email,
            'usermessage' => $req->message
        ];
        
        Mail::send('site.about.email', $values, function($m) use($req){
           $m->to('user1@localhost', 'User1');
           $m->subject('Contact Form from ' . $req->name);
        });
        
        $message = config('messages.contacts-success-send');
        Session::set('success', $message);
        return view ('site/about/contact-message');
    }
}