<?php  namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;

class AdminController extends Controller {

    /**
     * Initializer.
     *
     * @return \AdminController
     */
    public function __construct()
    {
        parent::__construct();
    }

}