<?php


namespace JPAPhotography\Http\Controllers;

class AdminDashboardController extends AdminController {

    /**
     * Admin dashboard
     */
    public function getIndex() {
        return view('admin/dashboard');
    }
}