<?php

namespace JPAPhotography\Http\Controllers\Auth;

use JPAPhotography\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class AuthController extends Controller {
    
    /*
     * |--------------------------------------------------------------------------
     * | Registration & Login Controller
     * |--------------------------------------------------------------------------
     * |
     * | This controller handles the registration of new users, as well as the
     * | authentication of existing users. By default, this controller uses
     * | a simple trait to add these behaviors. Why don't you explore it?
     * |
     */
    
    use AuthenticatesAndRegistersUsers;
    
    protected $redirectPath = '/';
    protected $redirectTo = '/';
    protected $loginPath = '/auth/login';
    // , ThrottlesLogins;
    
    /**
     * Create a new authentication controller instance.
     *
     * @param \Illuminate\Contracts\Auth\Guard $auth            
     * @param \Illuminate\Contracts\Auth\Registrar $registrar            
     * @return void
     */
    public function __construct() {
        //$this->auth = $auth;
        //$this->registrar = $registrar;
        $this->redirectPath = '/';
        $this->redirectTo = '/';
        $this->loginPath = '/auth/login';
        $this->middleware('guest', [ 'except' => 'getLogout' ]);
    }
    
    /**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
		    'user_login' => 'required|max:255|unique:users',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		], [
		    'user_login.required' => 'Username is required.',
            'user_login.unique' => 'The username has already been taken.'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
            'user_login' => $data['user_login'],
			'email' => $data['email'],
		    'user_status' => 1,
		    'display_name' => $data['name'],
			'password' => bcrypt($data['password']),
		]);
	}
}
