<?php

namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;
use JPAPhotography\Models\Post;
use JPAPhotography\Models\User;
use JPAPhotography\Utils\FlickrUtil;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use JPAPhotography\Models\Comment;
use Illuminate\Support\Facades\Log;
use JPAPhotography\Utils\LogUtil;
use JPAPhotography\Http\Requests\CreateBlogFormRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class BlogController extends Controller {
    
    /**
     * Post Model
     *
     * @var Post
     */
    protected $post;
    
    /**
     * User Model
     *
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     *
     * @param Post $post            
     * @param User $user            
     */
    public function __construct(Post $post, User $user) {
        parent::__construct();
        
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Returns all the blog posts.
     *
     * @return View
     */
    public function getIndex(Request $request) {
        $page = $request->input('page', 1);
        //$startTime = microtime();
        $posts = null;
        $cache_key = 'blog_posts_' . $page;
        
        if(Cache::has($cache_key)) {
            $posts = Cache::get($cache_key);
        } else {
            $posts = Post::where('post_status', '=', 'publish')
                     ->orderBy('post_date', 'DESC')
                     ->paginate(10);
                     
            $posts->load('metas', 'commentsCount');
            Cache::put($cache_key, $posts, 300);
        }
        
        $hasThumbnail = false;
        $view = view('site/blog/index', compact('posts', 'lastQuery', 'hasThumbnail'));
        //LogUtil::logTime('BlogController::getIndex', $startTime);
        return $view;
    }

    /**
     * View a blog post.
     *
     * @param string $slug            
     * @return View
     * @throws NotFoundHttpException
     */
    public function getView($post = 0, $post_name = '') {
        if(empty($post) && empty($post_name)) {
            abort(404);
            //return redirect()->action('BlogController@getIndex');
        }
        
        // Get this blog post data
        $post = Post::with('metas')->where('ID', '=', $post)->first();
        
        // Check if the blog post exists
        if (is_null($post)) {
            // If we ended up in here, it means that
            // a page or a blog post didn't exist.
            // So, this means that it is time for
            // 404 error page.
            return abort(404);
        }
        
        // Get this post comments
        $comments = $post->comments;//()->where('comment_post_id', '=', $post->ID)->get();//array();
        $comment_count = $comments->count();
        //$comments = Comment::with('author')->where('comment_post_id', '=', 1)->get();//$post->comments()->where('comment_post_id', '=', $post->id)->get(); // ->orderBy ( 'comment_date', 'ASC' )->get ();
        $thumbnail = '';
        foreach($post->metas as $meta) {
            if($meta->meta_key === 'thumbnail') {
                $thumbnail = $meta->meta_value;
                break;
            }
        }
           
        // Get current user and check permission
                                                // $user = $this->user->currentUser ();
        $canComment = false;
        // if (! empty ( $user )) {
        // $canComment = $user->can ( 'post_comment' );
        // }
        
        // Show the page
        return view('site/blog/view_post', compact('post', 'comments', 'canComment', 'thumbnail', 'comment_count'));
    }

    /**
     * View a blog post.
     *
     * @param string $slug            
     * @return Redirect
     */
    public function postView($slug) {
        $user = $this->user->currentUser();
        $canComment = $user->can('post_comment');
        if (! $canComment) {
            return Redirect::to($slug . '#comments')->with('error', 'You need to be logged in to post comments!');
        }
        
        // Get this blog post data
        $post = $this->post->where('slug', '=', $slug)->first();
        
        // Declare the rules for the form validation
        $rules = array (
            'comment' => 'required|min:3' 
        );
        
        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);
        
        // Check if the form validates with success
        if ($validator->passes()) {
            // Save the comment
            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->content = Input::get('comment');
            
            // Was the comment saved with success?
            if ($post->comments()->save($comment)) {
                // Redirect to this blog post page
                return Redirect::to($slug . '#comments')->with('alert-success', 'Your comment was added with success.');
            }
            
            // Redirect to this blog post page
            return Redirect::to($slug . '#comments')->with('alert-alert', 'There was a problem adding your comment, please try again.');
        }
        
        // Redirect to this blog post page
        return Redirect::to($slug)->withInput()->withErrors($validator);
    }
    
    public function getCreate(CreateBlogFormRequest $req) {
        return view('site/blog/create_post', compact('post', 'comments', 'canComment', 'thumbnail'));
    }
    
    public function postCreate(CreateBlogFormRequest $req) {
        return Redirect::to('/blog');
    }
}
