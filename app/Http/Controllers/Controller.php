<?php namespace JPAPhotography\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Cache;

abstract class Controller extends BaseController {

	use AuthorizesRequests, DispatchesCommands, ValidatesRequests;
	
	/**
	 * Initializer.
	 *
	 * @access   public
	 * @return \Controller
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => 'post'));
	}
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
