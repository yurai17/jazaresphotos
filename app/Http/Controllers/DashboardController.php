<?php
namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;
use JPAPhotography\Models\Post;
use JPAPhotography\Models\User;
use JPAPhotography\Models\Flickr;

use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex() {
        $users = User::orderBy('created_at')->take(5)->get();
        $posts = Post::orderBy('post_date')->take(5)->get();
		return view('site.dashboard.index', compact('users', 'posts'));
	}
    
    public function getUsersAdd() {
        return view('site.dashboard.users.add');
    }
    
    public function getPostsAdd() {
        return view('site.dashboard.posts.add');
    }
    
    public function postPostAdd(Request $req) {
        Cache::tags('blogs')->flush();//flushes only cache entries tagged with 'blogs'
        return view();
    }
    
    public function getUsers() {
        $users = Users::orderBy('created_at')->paginate(5);
        return view('site.dashboard.users.index', compact('users'));
    }
    
    public function getPhotos() {
        $images = Flickr::orderBy('flickr_id', 'desc')->paginate(8);
        return view('site.dashboard.photos.index', compact('images'));
    }
    
    public function postPhotosAdd() {
        Cache::tags('photos')->flush();//flushes only cache entries tagged with 'photos'
        return view('site.dashboard.photos.add');
    }
}
