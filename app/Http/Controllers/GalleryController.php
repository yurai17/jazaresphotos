<?php

namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use JPAPhotography\Utils\FlickrUtil;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use JPAPhotography\Models\Flickr;
use JPAPhotography\Models\JPAPhotography\Models;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class GalleryController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getIndex(Request $req) {
        $page = $req->input('page', 1);
        $cache_key = 'flickr_photos_' . $page;
        $images = Flickr::orderBy('date_taken', 'desc')->paginate(12);//null;
        
        /* if(Cache::has($cache_key)) {
            $images = Cache::get($cache_key);
        } else {
            $images = Flickr::orderBy('featured', 'desc')->orderBy('flickr_id', 'desc')->paginate(8);
            Cache::tags('photos')->put($cache_key, $images, 300);
        } */
        
        return view('site/portfolio/index', [ 'posts' => $images, 'page' => $req->input('page', 1) ]);
    }
    
    public function getFlickr(Request $req) {
        $page = $req->input('page', 1);
        
        $util = new FlickrUtil(null, 40, 10);
        $posts = null;
        $cache_key = 'flickr_photos_test_' . $page;
        if(Cache::has($cache_key)) {
            $posts = Cache::get($cache_key);
        } else {
            $posts = $util->fetchPhotos($page, 20);
            $posts->setPath(URL::to('portfolio/flickr'));
            Cache::put($cache_key, $posts, 300);
        }
        
        return view('site/portfolio/flickr', ['posts' => $posts]);
    }

    public function getIndextwo(Request $req) {
        $page = $req->input('page', 1); // $_GET['page'] == null ? 1 : intval($_GET['page']);
        $flUtil = new FlickrUtil(null, 40, 10);
        // if (! Cache::has ( 'Posts' )) {
        $posts = $flUtil->fetchPhotos($page, 20);
        // Cache::put ( 'Posts', $posts, 30 );
        // }
        $posts->setPath(URL::to('portfolio'));
        // $posts = Cache::get ( 'Posts' );
        
        if (! Cache::has('UserInfo')) {
            $userInfo = $flUtil->fetchUserInfo();
            Cache::put('UserInfo', $userInfo, 30);
        }
        
        $userInfo = Cache::get('UserInfo');
        
        // $posts = new LengthAwarePaginator($posts['photos']['photo'], 1, count($posts['photos']['photo']));
        
        return view('site/portfolio/indextwo', compact('posts', 'userInfo'));
    }

    public function getGallery(Request $req) {
        $page = $req->input('page', 1); // $_GET['page'] == null ? 1 : intval($_GET['page']);
        $flUtil = new FlickrUtil(null, 40, 10);
        // if (! Cache::has ( 'Posts' )) {
        $posts = $flUtil->fetchUserGalleries($page, 20);
        // Cache::put ( 'Posts', $posts, 30 );
        // }
        $posts->setPath(URL::to('portfolio'));
        
        return view('site/portfolio/gallery', compact('posts'));
    }

    public function getRefreshflickr(Request $req) {
        $pword = $req->input('pword', null);
        if($pword === null || $pword != 'gelatin') {
            abort(404);
            exit;
        }
        $flUtil = new FlickrUtil(null, 200, 10);
        for($i = 0; $i < 10; $i ++) {
            $posts = $flUtil->fetchPhotos($i, 200);
            
            //echo '<pre>';
            //dd($posts);
            //echo '</pre>';
            
            foreach ($posts as $post) {
                $flickr = new Flickr();
                try {
                    $flickr = new Flickr();
                    $flickr->flickr_id = $post ['id'];
                    $flickr->secret = $post ['secret'];
                    $flickr->title = $post ['title'];
                    $flickr->date_upload = $post['dateupload'];
                    $flickr->date_taken = $post['datetaken'];
                    $flickr->url_sq = $post ['url_sq'];
                    $flickr->height_sq = $post ['height_sq'];
                    $flickr->width_sq = $post ['width_sq'];
                    $flickr->url_l = $post ['url_l'];
                    $flickr->height_l = $post ['height_l'];
                    $flickr->width_l = $post ['width_l'];
                    $flickr->url_q = $post ['url_q'];
                    $flickr->height_q = $post ['height_q'];
                    $flickr->width_q = $post ['width_q'];
                    $flickr->url_o = $post ['url_o'];
                    $flickr->height_o = $post ['height_o'];
                    $flickr->width_o = $post ['width_o'];
                    $flickr->save();
                } catch (\PDOException $e) {
                    // DB::rollBack();
                    Log::error($e->getMessage());
                } catch (QueryException $e) {
                    // DB::rollBack();
                    Log::error($e->getMessage());
                } catch (\Exception $e) {
                    // DB::rollBack();
                    Log::error($e->getMessage());
                }
            }
        }
    }

    public function getClearcache(Request $req) {
        $pword = $req->input('pword', '');
        if($pword !== 'gelatin') {
            abort(404);
            exit;
        }
        Cache::forget('Posts');
        Cache::forget('UserInfo');
    }
}