<?php

namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Models\Flickr;

use Illuminate\Http\Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex(Request $req) {
        $page = $req->input('page', 1);
        $cache_key = 'flickr_photos_' . $page;
        $images = Flickr::orderBy('featured', 'desc')->orderBy('date_taken', 'desc')->paginate(1);//null;
        
        /* if(Cache::has($cache_key)) {
            $images = Cache::get($cache_key);
        } else {
            $images = Flickr::orderBy('featured', 'desc')->orderBy('flickr_id', 'desc')->paginate(8);
            Cache::tags('photos')->put($cache_key, $images, 300);
        } */
        
        return view('home', [ 'posts' => $images, 'page' => $page ]);
	}
}
