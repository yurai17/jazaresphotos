<?php

namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JPAPhotography\Utils\Object;
use JPAPhotography\Models\Flickr;

class PhotoController extends Controller {
    public function getView($id = '') {
        if(!is_numeric($id)) {
            return redirect()->action('PortfolioController@getIndex');
        }
        
        $post = Flickr::where('flickr_id', $id)->first();
        if(Object::isNull($post)) {
            abort(404);
        }
        return view('site/photo/view', compact('post'));
    }
}
