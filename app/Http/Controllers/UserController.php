<?php namespace JPAPhotography\Http\Controllers;

use JPAPhotography\Http\Controllers\Controller;
use JPAPhotography\Models\User;
use JPAPhotography\Models\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JPAPhotography\Http\Requests\LoginFormRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Mail\Message;

class UserController extends Controller {
    use ThrottlesLogins;
    
    protected $username = null; //used by ThrottlesLogins
    protected $loginPath = '/user/login'; //used by ThrottlesLogins
    protected $maxLoginAttempts = 3; //used by ThrottlesLogins
    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * Inject the models.
     * @param User $user
     * @param UserRepository $userRepo
     */
    public function __construct(User $user, UserRepository $userRepo)
    {
        parent::__construct();
        $this->user = $user;
        $this->userRepo = $userRepo;
    }
    
    /**
     * Used by ThrottlesLogins
     */
    protected function loginUsername() {
        return $this->username;
    }
    
    /**
     * Used by ThrottlesLogins
     */
    protected function loginPath() {
        return $this->loginPath;
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex($user_id) {
        $user = User::where('ID', '=', $user_id)->first();
        
        return view('site/user/index', compact('user'));
    }

    /**
     * Stores new user
     *
     */
    public function postIndex()
    {
        $user = $this->userRepo->signup(Input::all());

        if ($user->id) {
            if (Config::get('confide::signup_email')) {
                Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                );
            }

            return Redirect::to('user/login')
                ->with('success', Lang::get('user/user.user_account_created'));
        } else {
            $error = $user->errors()->all(':message');

            return Redirect::to('user/create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }

    }
    
    public function getEdit($user_id, Request $req) {
        if(!auth()->check()) {
            return redirect('/user/login');
        }
        
        $user = User::where('ID', '=', $user_id)->first();
        
        return view('site/user/edit', compact('user'));
    }

    /**
     * Edits a user
     * @var User
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(User $user)
    {
        $oldUser = clone $user;

        $user->username = Input::get('username');
        $user->email = Input::get('email');

        $password = Input::get('password');
        $passwordConfirmation = Input::get('password_confirmation');

        if (!empty($password)) {
            if ($password != $passwordConfirmation) {
                // Redirect to the new user page
                $error = 'Passwords do not match';//Lang::get('admin/users/messages.password_does_not_match');
                return Redirect::to('user')
                    ->with('error', $error);
            } else {
                $user->password = $password;
                $user->password_confirmation = $passwordConfirmation;
            }
        }

        if ($this->userRepo->save($user)) {
            return Redirect::to('user')
                ->with( 'success', Lang::get('user/user.user_account_updated') );
        } else {
            $error = $user->errors()->all(':message');
            return Redirect::to('user')
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', $error);
        }

    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {
        return View::make('site/user/create');
    }


    /**
     * Displays the login form
     *
     */
    public function getLogin()
    {
        if(auth()->check()){
            return Redirect::to('/');
        }

        return view('site/user/login');
        //return view('login');
    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin(LoginFormRequest $req) {
        $input = Input::all();
        
        $params = array ('password' => $input['password'], 'user_status' => 1);
        $this->username = $input['login'];
        
        if(!filter_var($input['login'], FILTER_VALIDATE_EMAIL)) {
            $params['user_login'] = $input['login'];
        } else {
            $params['email'] = $input['login'];
        }
        
        if($this->hasTooManyLoginAttempts($req)) {
            return $this->sendLockoutResponse($req);
        }
        
        if (Auth::attempt($params, $req->has('remember'))) {
            $this->clearLoginAttempts($req);
            return Redirect::intended('/');
        }
        
        $this->incrementLoginAttempts($req);
        
        return Redirect::to('user/login')
                ->withInput(Input::except('password'))
                ->with('error', 'Invalid credentials');
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getConfirm($code)
    {
        if ( Confide::confirm( $code ) )
        {
            return Redirect::to('user/login')
                ->with( 'notice', Lang::get('confide::confide.alerts.confirmation') );
        }
        else
        {
            return Redirect::to('user/login')
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_confirmation') );
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot() {
        if(auth()->check()) {
            return redirect('/');
        }
        return view('site/user/forgot');
    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgot(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject('Your password reset link.');
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('alert-success', trans($response));

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset( $token )
    {
        return View::make('site/user/reset')
            ->with('token',$token);
    }


    /**
     * Attempt change password of the user
     *
     */
    public function postReset()
    {

        $input = array(
            'token'                 =>Input::get('token'),
            'password'              =>Input::get('password'),
            'password_confirmation' =>Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($this->userRepo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::to('user/login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::to('user/reset', array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }

    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {
        Confide::logout();

        return Redirect::to('/');
    }

    /**
     * Get user's profile
     * @param $username
     * @return mixed
     */
    public function getProfile($username)
    {
        $userModel = new User;
        $user = $userModel->getUserByUsername($username);

        // Check if the user exists
        if (is_null($user))
        {
            return App::abort(404);
        }

        return View::make('site/user/profile', compact('user'));
    }

    public function getSettings()
    {
        list($user,$redirect) = User::checkAuthAndRedirect('user/settings');
        if($redirect){return $redirect;}

        return View::make('site/user/profile', compact('user'));
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1,$url2,$url3)
    {
        $redirect = '';
        if( ! empty( $url1 ) )
        {
            $redirect = $url1;
            $redirect .= (empty($url2)? '' : '/' . $url2);
            $redirect .= (empty($url3)? '' : '/' . $url3);
        }
        return $redirect;
    }
    
    public function getEncryptUserPword(Request $req) {
        $pword = $req->input('pword', 'test');
        $cipher = Hash::make($pword);
    
        return view('site/portfolio/pword', compact('cipher'));
    }
    
    public function getDashboard(Request $req) {
        return view('site/user/dashboard');
    }
}
