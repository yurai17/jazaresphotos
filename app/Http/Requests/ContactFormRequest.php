<?php

namespace JPAPhotography\Http\Requests;

use JPAPhotography\Http\Requests\Request;

class ContactFormRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [ 
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ];
    }
    
    public function messages() {
        return [
            'name.required' => 'Name cannot be empty.',
            'email.required' => 'Email cannot be empty.',
            'email.email' => 'Invalid email format.',
            'message.required' => 'Message cannot be empty.',
            'g-recaptcha-response.required' => 'Invalid Captcha.'
        ];
    }
}