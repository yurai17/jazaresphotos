<?php

namespace JPAPhotography\Http\Requests;

use JPAPhotography\Http\Requests\Request;
use Auth;

class CreateBlogFormRequest extends Request {

    public function authorize() {
        return Auth::check();
    }

    public function rules() {
        if(request()->isMethod('post')) {
            return [ 
                'title' => 'required',
                'content' => 'required'
            ];
        }
        return [];
    }
    
    public function messages() {
        return [
            'title.required' => 'Title cannot be empty.',
            'content.required' => 'Content cannot be empty.'
        ];
    }
}