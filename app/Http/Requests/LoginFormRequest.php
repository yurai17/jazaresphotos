<?php

namespace JPAPhotography\Http\Requests;

use JPAPhotography\Http\Requests\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Auth;

class LoginFormRequest extends Request {
    use ThrottlesLogins;
    
    protected $username = null;
    protected $loginPath = '/user/login';
    protected $maxLoginAttempts = 3;
    
    protected function loginUsername() {
        return $this->username;
    }
    
    public function authorize() {
        return !Auth::check();
    }
    
    public function rules() {
        return [
            'login' => 'required',
            'password' => 'required'
        ];
    }
    
    public function messages() {
        return [
            'login.required' => 'Username or Email cannot be empty.',
            'Password' => 'Password cannot be empty',
        ];
    }
    
    public function validateLogin($input) {
        $request = request();
        
        $result = array(
            'error' => 0,
            'messages' => array(),
        );
        
        $params = array ('password' => $input['password'] );
        $this->username = $input['login'];
        
        if(!filter_var($input['login'], FILTER_VALIDATE_EMAIL)) {
            $params['user_login'] = $input['login'];
        } else {
            $params['email'] = $input['login'];
        }
        
        if($this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }
        
        if (Auth::attempt($credentials, $request->has('remember'))) {
            $this->clearLoginAttempts($request);
        }
        
        $this->incrementLoginAttempts($request);
    }
}