<?php

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
*  ------------------------------------------
*/
//Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
*/
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('page', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('photo_id', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');
Route::pattern('post_name', '[0-9a-z]+');
/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
*/
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

	# Comment Management
	Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
	Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
	Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
	Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
	Route::controller('comments', 'AdminCommentsController');

	# Blog Management
	Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
	Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
	Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
	Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
	Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete');
	Route::controller('blogs', 'AdminBlogsController');

	# User Management
	Route::get('users/{user}/show', 'AdminUsersController@getShow');
	Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
	Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
	Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
	Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
	Route::controller('users', 'AdminUsersController');

	# User Role Management
	Route::get('roles/{role}/show', 'AdminRolesController@getShow');
	Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
	Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
	Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
	Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
	Route::controller('roles', 'AdminRolesController');

	# Admin Dashboard
	Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
*/

// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::get('user/{user}', 'UserController@getIndex');
Route::get('user/{user}/edit', 'UserController@getEdit');
Route::post('user/{user}/edit', 'UserController@postEdit');
Route::post('user/login', 'UserController@postLogin');
# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

# Filter for detect language
Route::when('contact-us','detectLang');

# Contact Us Static Page
Route::get('contact-us', function()
{
	// Return about us page
	return View::make('site/contact-us');
});

# Posts - Second to last set, match slug
// Route::get('{postSlug}', 'BlogController@getView');
// Route::post('{postSlug}', 'BlogController@postView');

Route::controller('gallery', 'GalleryController');
Route::controller('blog', 'BlogController');
Route::get('blog/view/{post}/{post_name}', 'BlogController@getView');
Route::get('photo/view/{photo_id}', 'PhotoController@getView');
Route::controller('photo', 'PhotoController');
Route::controller('about', 'AboutController');

Route::group(array('prefix'=>'dashboard'), function() {
    Route::get('users/add', 'DashboardController@getUsersAdd');
    Route::get('posts/add', 'DashboardController@getPostsAdd');
    Route::controller('/', 'DashboardController');
});

# Index Page - Last route, no matches
Route::get('page/{page}', 'HomeController@getIndex');
Route::get('/', array('before' => 'detectLang','uses' => 'HomeController@getIndex'));
//Route::get('/', array('before' => 'detectLang','uses' => 'PortfolioController@getIndex'));

Event::listen('illuminate.query', function($query, $params) {
    $debug = Input::get('debug');
    if(Config::get('app.debug') == '1' && $debug === 'true') {
        echo '<pre>';
        var_dump($query);
        var_dump($params);
        echo '</pre>';
    }
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//Route::get('/', 'WelcomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);
