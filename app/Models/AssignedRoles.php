<?php

namespace JPAPhotography\Models;

use Illuminate\Database\Eloquent\Model;

class AssignedRoles extends Model {
    protected $table = 'assigned_roles';
    
    public function roles() {
        return $this->hasMany('JPAPhotography\Models\Role', 'id', 'role_id');
    }
    
    public function permissions() {
        return $this->hasMany('JPAPhotography\Models\Permission', 'id', 'role_id');
    }
}