<?php

namespace JPAPhotography\Models;

use Illuminate\Database\Eloquent\Model;

class Flickr extends Model {
    protected $table = 'flickr_images';
    public $timestamps = true;
    
    public function getDateUploadAttribute($value) {
        return date('D M d, Y g:i A', $value);
    }
    
    public function getDateTakenAttribute($value) {
        return date_format(date_create($value), 'D M d, Y');
    }
    
    public function getImages() {
        return $this->all();
    }
}