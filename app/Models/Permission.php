<?php

namespace JPAPhotography\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {
    protected $table = 'permissions';
    
    public function permissions() {
        return $this->hasMany('JPAPhotography\Models\Role', 'id', 'role_id');
    }
}