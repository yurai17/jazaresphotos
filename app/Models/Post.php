<?php

namespace JPAPhotography\Models;

use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    protected $table = 'posts';
    
    public function getPostDateAttribute($value) {
        return date_format(date_create($value), 'D M d, Y g:i A');
    }
    
    /**
     * Deletes a blog post and all
     * the associated comments.
     *
     * @return bool
     */
    public function delete() {
        // Delete the comments
        $this->comments()->delete();
        
        // Delete the blog post
        return parent::delete();
    }

    /**
     * Returns a formatted post content entry,
     * this ensures that line breaks are returned.
     *
     * @return string
     */
    public function content() {
        return nl2br($this->content);
    }

    /**
     * Get the post's author.
     *
     * @return User
     */
    public function author() {
        return $this->belongsTo('JPAPhotography\Models\User', 'post_author', 'ID');
    }

    /**
     * Get the post's meta_description.
     *
     * @return string
     */
    public function meta_description() {
        return $this->meta_description;
    }

    /**
     * Get the post's meta_keywords.
     *
     * @return string
     */
    public function meta_keywords() {
        return $this->meta_keywords;
    }

    public function metas() {
        return $this->hasMany('JPAPhotography\Models\PostMeta', 'post_id', 'ID');
    }

    /**
     * Get the post's comments.
     *
     * @return array
     */
    public function comments() {
        return $this->hasMany('JPAPhotography\Models\Comment', 'comment_post_ID', 'ID');
    }
    
    public function commentsCount() {
        return $this->hasOne('JPAPhotography\Models\Comment', 'comment_post_ID', 'ID')
               ->selectRaw('comment_post_ID, count(*) as aggregate')
               ->groupBy('comment_post_ID');
    }

    public function getCommentsCountAttribute() {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('commentsCount', $this->relations)) 
            $this->load('commentsCount');

        $related = $this->getRelation('commentsCount');

        // then return the count directly
        return ($related) ? (int) $related->aggregate : 0;
    }

    /**
     * Get the date the post was created.
     *
     * @param \Carbon|null $date            
     * @return string
     */
    public function date($date = null) {
        if (is_null($date)) {
            $date = $this->created_at;
        }
        
        return String::date($date);
    }

    /**
     * Get the URL to the post.
     *
     * @return string
     */
    public function url() {
        return Url::to($this->slug);
    }

    /**
     * Returns the date of the blog post creation,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function created_at() {
        return $this->date($this->created_at);
    }

    /**
     * Returns the date of the blog post last update,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function updated_at() {
        return $this->date($this->updated_at);
    }
}
