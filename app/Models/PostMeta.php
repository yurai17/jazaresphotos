<?php

namespace JPAPhotography\Models;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model {
    protected $table = 'postmeta';
}