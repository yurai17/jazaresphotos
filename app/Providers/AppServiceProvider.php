<?php namespace JPAPhotography\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use JPAPhotography\Utils\SessionUtil;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
	    //Event::listen('auth.logout', '\JPAPhotography\Utils\SessionUtil');
		//Blade::directive('break', function() {
		  //  return "<?php break; ";
		//});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'JPAPhotography\Services\Registrar',
		    'JPAPhotography\Utils\SessionUtil'
		);
	}

}
