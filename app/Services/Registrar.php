<?php namespace JPAPhotography\Services;

use JPAPhotography\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
		    'user_login' => 'required|max:255|unique:users',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		], [
		    'user_login.required' => 'Username is required.',
            'user_login.unique' => 'The username has already been taken.'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
            'user_login' => $data['user_login'],
			'email' => $data['email'],
		    'user_status' => 1,
		    'display_name' => $data['name'],
			'password' => bcrypt($data['password']),
		]);
	}

}
