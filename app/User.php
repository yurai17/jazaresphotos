<?php


namespace JPAPhotography;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use JPAPhotography\Models\Permission;
use Illuminate\Support\Facades\DB;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {
    
    use Authenticatable, Authorizable, CanResetPassword;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $primaryKey = 'ID';

    public function getKey() {
        return $this->ID;
    }

    public function assignedRoles() {
        return $this->hasMany('\JPAPhotography\Models\AssignedRoles', 'user_id', 'ID');
    }
    
    public function permissions() {
        return $this->hasManyThrough('\JPAPhotography\Models\Permission', '\JPAPhotography\Models\AssignedRoles');
    }
    
    public function hasRole($role) {
        $roles = $this->assignedRoles;
//         $permissions = $this->permissions;
        //$permissions = DB::select('select a.name from permission_role b left join permissions a on b.permission_id = a.id where role_id in (2)');
//         dd($permissions);
        foreach ($roles as $role) {
            if($role->role_id == 1) {
                return true;
                break;
            }
        }
        return false;
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','user_login','email','password' ];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [ 'password','remember_token' ];
}
