<?php


namespace JPAPhotography\Utils;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Countable;
use ArrayAccess;
use IteratorAggregate;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Pagination\BootstrapThreePresenter;

class FlickrUtil extends LengthAwarePaginator {
    
    /**
     * url_sq = 75x75
     * url_t = 100x75
     * url_s = 240x180
     * url_q = 150x150
     * url_m = 500x375
     * url_n = 320x320
     * url_z = 640x480
     * url_c = 800x600
     * url_l = 1024x768
     * url_o = original size
     */
    var $_key = '755b5336f8b95f07ee4649aaede7f9ab';
    var $_secret = '40adccbdbf4ccc8e';
    var $_nsid = '129883707@N02';
    var $_username = 'jobert.azares';

    public function fetchPhotos($page = 1, $per_page = 10) {
        $params = array (
                        'user_id' => $this->_nsid,
                        'per_page' => $per_page, // max is 500
                        'extras' => 'date_upload,date_taken,url_sq,url_l,url_o,url_q,o_dims,path_alias,owner_name',
                        'page' => $page 
        );
        $reqUrl = $this->buildRequestUrl ( 'flickr.photos.search', 'php_serial', $params );
        // echo $reqUrl;
        $response = $this->callREST ( $reqUrl );
        
        $this->total = intval ( $response ['photos'] ['total'] );
        $this->perPage = $per_page;
        $this->lastPage = ( int ) ceil ( $this->total / $per_page );
        $this->currentPage = $this->setCurrentPage ( $page, $this->lastPage );
        $this->items = $response ['photos'] ['photo'] instanceof Collection ? $response ['photos'] ['photo'] : Collection::make ( $response ['photos'] ['photo'] );
        
        if ($this->currentPage != $this->lastPage) {
        }
        
        return $this;
    }
    
    public function fetchUserGalleries($page = 1, $per_page = 10) {
        $params = array (
                        'user_id' => $this->_nsid,
                        'per_page' => $per_page, // max is 500
                        'primary_photo_extras' => 'date_upload,date_taken,url_sq,url_l,url_o,url_q,o_dims,path_alias,owner_name',
                        'page' => $page
        );
        $reqUrl = $this->buildRequestUrl ( 'flickr.galleries.getList', 'php_serial', $params );
        // echo $reqUrl;
        $response = $this->callREST ( $reqUrl );
        
        $this->total = intval ( $response ['galleries'] ['total'] );
        $this->perPage = $per_page;
        $this->lastPage = ( int ) ceil ( $this->total / $per_page );
        $this->currentPage = $this->setCurrentPage ( $page, $this->lastPage );
        $this->items = $response ['galleries'] ['gallery'] instanceof Collection ? $response ['galleries'] ['gallery'] : Collection::make ( $response ['galleries'] ['gallery'] );
        
        if ($this->currentPage != $this->lastPage) {
        }
        
        return $this;
    }

    public function fetchUserInfo($user_id = '129883707@N02') {
        $params = array (
                        'user_id' => $user_id 
        );
        $reqUrl = $this->buildRequestUrl ( "flickr.people.getInfo", 'php_serial', $params );
        
        $response = $this->callREST ( $reqUrl );
        
        return $response;
    }

    public function callREST($url) {
        $response = file_get_contents ( $url );
        $response = unserialize ( $response );
        return $response;
    }

    /**
     * Render the paginator using the given presenter.
     *
     * @param \Illuminate\Contracts\Pagination\Presenter|null $presenter            
     * @return string
     */
    public function render(Presenter $presenter = null) {
        if (is_null ( $presenter ) && static::$presenterResolver) {
            $presenter = call_user_func ( static::$presenterResolver, $this );
        }
        
        $presenter = $presenter ?  : new BootstrapThreePresenter( $this );
        
        return $presenter->render ();
    }

    function buildRequestUrl($method, $format = 'php_serial', $params) {
        $params ['method'] = $method;
        $params ['format'] = $format;
        $params ['api_key'] = $this->_key;
        $encoded_params = array ();
        foreach ( $params as $k => $v ) {
            $encoded_params [] = urlencode ( $k ) . '=' . urlencode ( $v );
        }
        
        $reqUrl = 'https://api.flickr.com/services/rest/?' . implode ( '&', $encoded_params );
        return $reqUrl;
    }
}