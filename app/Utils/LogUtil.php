<?php

namespace JPAPhotography\Utils;

use Illuminate\Support\Facades\Log;
class LogUtil {
    public static function enterMethod($methodName) {
        Log::info('+ ' . $methodName);
    }
    
    public static function exitMethod($methodName) {
        Log::info('- ' . $methodName);
    }
    
    public static function logTime($label, $startTime, $endTime = null) {
        if(empty($endTime)) {
            $endTime = microtime();
        }
        $elapsedTime = $endTime - $startTime;
        
        Log::info(implode(': ', [$label, 'Elapsed', $elapsedTime]));
    }
}