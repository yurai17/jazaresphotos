<?php

namespace JPAPhotography\Utils;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use JPAPhotography\User;
use Illuminate\Support\Facades\Log;

class SessionUtil {

    public static function user() {
        LogUtil::enterMethod('SessionUtil::user()');
        
        $user = empty(Session::get('user')) ? null : Session::get('user');
        
        LogUtil::exitMethod('SessionUtil::user() return ');
        return $user;
    }
    
    public static function isAdmin() {
        $user = Session::get('user');
        if(array_has($user, 'roles')) {
            $roles = $user['roles'];
            if(in_array('admin', $roles)) {
                return true;
            }
        }
        
        return false;
    }

    public static function check() {
        LogUtil::enterMethod('SessionUtil::check()');
        
        $user = Session::get('user');
        $isPresent = !empty($user);
        
        LogUtil::exitMethod('SessionUtil::check() return ' . strval($isPresent));
        return $isPresent;
    }
}