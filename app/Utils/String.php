<?php

namespace JPAPhotography\Utils;

class String {
    public static function isNull($string) {
        return is_null($string);
    }
    
    public static function abortIfNull($string) {
        if(is_null($string)) {
            abort(404);
        }
    }
    
    public static function defaultIfNull($string, $defaultValue) {
        if(is_null($string) && !is_null($defaultValue)) {
            return $defaultValue;
        } elseif (!is_null($string)) {
            return $string;
        } else {
            return '';
        }
    }
}