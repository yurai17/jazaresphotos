<!DOCTYPE html>
<html lang="en">
    @include('site/flayout/header')
    
    <body>
        <!-- Navbar -->
        @include('site/flayout/navigation')
        <!-- ./ navbar -->

        <!-- To make sticky footer need to wrap in a div -->
        <div id="wrap">
            <!-- Container -->
            <div class="container-fluid">
                <!-- Notifications -->
                @include('site/layouts/notifications')
                <!-- ./ notifications -->

                <!-- Content -->
                @yield('content')
                <!-- ./ content -->
            </div>
            <!-- ./ container -->

            <!-- the following div is needed to make a sticky footer -->
            <div id="push"></div>
        </div>
        <!-- ./wrap -->

        @include('site/flayout/footer')
        <!--pre>{{-- var_dump(Session::all()) --}}</pre-->

        <!-- Javascripts
        ================================================== -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/vendor/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/vendor/what-input.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/foundation.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
        @yield('scripts')
        
        <script>
            $(document).ready(function() {
                $(document).foundation();
            });
            
            @section('inline_scripts')
            @show
        </script>
    </body>
</html>
