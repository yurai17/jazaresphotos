@extends('site.' . config('app.layout') . '.default')

@section('scripts_nothing')
<script src="{{ asset('/js/StackBlur.js') }}"></script>
@stop

@section('inline_scripts_nothing')
    var canvas = document.getElementById("galleryCanvas");
    var canvasContext = canvas.getContext("2d");
    var thisImage = document.getElementById("mainPhotoImage");
    var canvasBackground = new Image();
    canvasBackground.src = thisImage.src;
    
    var drawBlur = function() {
        var w = canvas.width;
        var h = canvas.height;
        canvasContext.drawImage(canvasBackground, 0, 0, w, h);
        //stackBlurCanvasRGBA("galleryCanvas", 0, 0, w, h, 100);
    }
    
    canvasBackground.onload = function() {
        drawBlur();
    }
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('/css/home.css') }}" />
@stop

@section('content')
    <div class="gallery">
        <svg class="gallery_background">
            <defs>
                <filter id="blur">
                    <feGaussianBlur stdDeviation="10" />
                </filter>
            </defs>
            <image id="mainPhotoImage"
                   filter="url(#blur)"
                   data-caption="{{ $posts[0]->title }}"
                   width="100%"
                   height="100%"
                   xlink:href="{{ $posts[0]->url_l }}" itemprop="thumbnailUrl"></image>
        </svg>
        <br/>
        <div id="pagination-wrapper" class="row text-center pagination-wrapper">
            @include('site.flayout.pagination-short', ['results' => $posts])
        </div>
        
        <div class="row">
            <ul class="clearing-thumbs no-bullet" data-clearing>
                @foreach($posts as $post)
                    <li class="item-wrapper large-12 small-centered columns">
                        <div class="img-wrapper text-center" itemid="{{ $post->url_q }}" itemscope itemtype="http://schema.org/Photograph">
                            <div itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                <a itemprop="contentUrl" class="thumbnail radius" data-open="carousel-modal" href="{{ $post->url_o }}" target="_blank">
                                    <img id="mainPhotoImage"
                                            filter="url(#blur)"
                                            data-caption="{{ $posts[0]->title }}"
                                            width="{{ $posts[0]->width_l }}" 
                                            height="{{ $posts[0]->height_l }}" 
                                            src="{{ $posts[0]->url_l }}" itemprop="thumbnailUrl"/>
                                    <div class="title">
                                        <h2 class="vertical-center" itemprop="caption"><strong>{{ $post->title }}</strong></h2>
                                        <p class="vertical-center" itemprop="dateTaken"><em>{{ $post->date_taken }}</em></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        
        <div id="pagination-wrapper" class="row text-center pagination-wrapper">
            @include('site.flayout.pagination-short', ['results' => $posts])
        </div>
    </div>
@stop