@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

@section('content')
<form role="form" method="POST" action="{{{ URL::to('/user/login') }}}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
    <fieldset>
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" tabindex="1" placeholder="Email or username" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
        </div>
        <div class="form-group">
        <label for="password">
            Password
        </label>
        <input class="form-control" tabindex="2" placeholder="Password" type="password" name="password" id="password">
        <p class="help-block">
            <a href="{{{ URL::to('/users/forgot_password') }}}">Forgot password?</a>
        </p>
        </div>
        <div class="checkbox">
            <label for="remember">
                <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> Remember me
            </label>
        </div>
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
        @endif

        @if (Session::get('notice'))
            <div class="alert">{{{ Session::get('notice') }}}</div>
        @endif
        <div class="form-group">
            <button tabindex="3" type="submit" class="btn btn-default">Submit</button>
        </div>
    </fieldset>
</form>
@stop