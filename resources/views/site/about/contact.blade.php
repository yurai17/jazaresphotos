@extends('site.' . config('app.layout') . '.default')

@section('title')
Contact us :: @parent
@stop

@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
@stop

@section('inline_scripts')
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.name = null;
    $scope.email = null;
    $scope.message = null;
});
@stop

@section('content')
<br/>
<div class="row">
    <div class="medium-10 small-centered columns" ng-app="myApp" ng-controller="validateCtrl">
        <h2 class="text-center"><strong>Contact Us</strong></h2>
        {!! Form::open(array('url' => '/about/contact-us', 'role' => 'form', 'name'=>'contactForm', 'novalidate')) !!}
            <div class="form-group">
                <label for="name">Your Name</label>
                <input type="text" name="name" class="form-control" placeholder="Your name" ng-model="name" required/>
                <span class="alert label error-span expanded" ng-show="contactForm.name.$invalid">
                    <span ng-show="contactForm.name.$error.required"><i class="fi-x-circle"></i>&nbsp;Name is required.</span>
                </span>
            </div>
            
            <div class="form-group">
                <label for="email">Your E-mail Address</label>
                <input type="email" name="email" class="form-control" placeholder="Your e-mail address" ng-model="email" required />
                <span class="alert label error-span" ng-show="contactForm.email.$invalid">
                    <span ng-show="contactForm.email.$error.required"><i class="fi-x-circle"></i>&nbsp;Email is required.</span>
                    <span ng-show="contactForm.email.$error.email"><i class="fi-x-circle"></i>&nbsp;Invalid email address.</span>
                </span>
            </div>
            
            <div class="form-group">
                <label for="message">Your Message</label>
                <textarea name="message" class="form-control" placeholder="Your message" rows="5" ng-model="message" required></textarea>
                <span class="alert label error-span" ng-show="contactForm.message.$invalid">
                    <span ng-show="contactForm.message.$error.required"><i class="fi-x-circle"></i>&nbsp;Message is required.</span>
                </span>
            </div>
            
            <div class="form-group">
                {!! Form::captcha() !!}
            </div>
            
            <div class="form-group">
                <input class="button radius" ng-disabled="(contactForm.name.$invalid) || (contactForm.email.$dirty &amp;&amp; contactForm.email.$invalid) || (contactForm.message.$invalid)" value="Contact Us!" type="submit" />
            </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
