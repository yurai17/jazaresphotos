@extends('site.' . config('app.layout') . '.default')

@section('title')
About us :: @parent
@stop

@section('content')
<br/>
<div class="row">
    <div class="medium-10 small-centered columns">
        <h2 class="text-center"><strong>About Us</strong></h2>
        
        <p>Don't be bashful drop me a line. I can't think of anything more rewarding than being able to express yourself to others through painting. And right there you got an almighty cloud. Maybe we got a few little happy bushes here, just covered with snow. We'll put all the little clouds in and let them dance around and have fun. Use your imagination, let it go.</p>

        <p>If you didn't have baby clouds, you wouldn't have big clouds. Go out on a limb - that's where the fruit is. I thought today we would make a happy little stream that's just running through the woods here. The first step to doing anything is to believe you can do it. See it finished in your mind before you ever start.</p>

        <p>Now we don't want him to get lonely, so we'll give him a little friend. If you've been in Alaska less than a year you're a Cheechako. I like to beat the brush. See. We take the corner of the brush and let it play back-and-forth. If you don't like it - change it. It's your world.</p>

        <p>We have a fantastic little sky! Only eight colors that you need. This is a happy place, little squirrels live here and play. Let's put some happy trees and bushes back in here. A tree cannot be straight if it has a crooked trunk.</p>

        <p>We'll put some happy little leaves here and there. Here's something that's fun. You can't have light without dark. You can't know happiness unless you've known sorrow. And just raise cain. If there's two big trees invariably sooner or later there's gonna be a little tree. You can create anything that makes you happy.</p>

        <p>These little son of a guns hide in your brush and you just have to push them out. Nice little fluffy clouds laying around in the sky being lazy. Just make little strokes like that.</p>

        <p>A beautiful little sunset. Absolutely no pressure. You are just a whisper floating across a mountain. Maybe there's a happy little bush that lives right there. Poor old tree. Everybody's different. Trees are different. Let them all be individuals. Only think about one thing at a time. Don't get greedy.</p>
        <br/>
    </div>
</div>
@endsection
