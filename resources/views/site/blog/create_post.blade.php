@extends('site.' . config('app.layout') . '.default')

{{-- Web site Title --}}
@section('title')
Create New Blog ::
@parent
@stop

@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "#content",
    theme: "modern",
    height: "600",
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor"
    ],
    content_css: "css/content.css",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons"
});

var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.title = null;
    $scope.content = null;
});
</script>
@stop

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="large-10 small-centered columns" ng-app="myApp" ng-controller="validateCtrl">
            <h2><strong>Create new blog post</strong></h2>
            
            {!! Form::open(array('url' => '/blog/create', 'role' => 'form', 'name'=>'createForm', 'novalidate')) !!}
                <div class="form-group">
                    {!! Form::label('Title') !!}
                    {!! Form::text('title', null, 
                        array('required','class'=>'form-control', 
                              'placeholder'=>'Title',
                              'ng-model'=>'title')) !!}
                    <span class="alert label error-span expanded" ng-show="createForm.title.$dirty && createForm.title.$invalid">
                        <span ng-show="createForm.title.$error.required"><i class="fi-x-circle"></i>&nbsp;Title is required.</span>
                    </span>
                </div>
                
                <div class="form-group">
                    {!! Form::label('Content') !!}
                    {!! Form::textarea('content', null, 
                        array('required','class'=>'form-control', 
                              'placeholder'=>'Your message',
                              'id'=>'content',
                              'rows'=>'10',
                              'ng-model'=>'content')) !!}
                    <span class="alert label error-span expanded" ng-show="createForm.content.$dirty && createForm.content.$invalid">
                        <span ng-show="createForm.content.$error.required"><i class="fi-x-circle"></i>&nbsp;Content is required.</span>
                    </span>
                </div>
                
                <div class="form-group">
                    {!! Form::submit('Submit', 
                      array('class'=>'button',
                       'show'=>'(createForm.content.$invalid) || (contactForm.title.$invalid)')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
