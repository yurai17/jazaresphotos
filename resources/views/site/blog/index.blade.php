@extends('site.' . config('app.layout') . '.default')

@section('title')
Blogs :: @parent
@stop

@section('content')
<br/>
<div id="pagination-wrapper" class="row text-center">
    @include('site.flayout.pagination-short', ['results' => $posts])
</div>
<div class="row">
    <div class="medium-9 small-centered columns">
        <?php $classThumbnail = ''; ?>
        @foreach ($posts as $post)
            <div itemid="{{ URL::to('/blog/view/' . $post->ID . '/' . $post->post_name) }}" itemscope itemtype="http://schema.org/BlogPosting">
                <!-- Post Title -->
                <h3 itemprop="headline"><strong><a href="/blog/view/{{{ $post->ID }}}/{{{ $post->post_name }}}">{{ $post->post_title }}</a></strong></h3>
                <!-- ./ post title -->

                <!-- Post Content -->
                <a href="/blog/view/{{{ $post->ID }}}/{{ $post->post_name }}" class="thumbnail">
                    <img itemprop="image" src="http://placehold.it/850x350" alt="{{ $post->post_title }}" />
                </a>
                <div itemprop="articleBody">
                {!! str_limit($post->post_content, 200) !!}
                </div>
                <p><a itemprop="url" class="small button radius" href="/blog/view/{{{ $post->ID }}}/{{{ $post->post_name }}}">Read more</a></p>
                <!-- ./ post content -->

                <!-- Post Footer -->
                <p>
                    <span class="fi-calendar"></span>&nbsp;<span itemprop="datePublished">{{{ $post->post_date }}}</span>
                    | <span class="fi-comments"></span>&nbsp;<a itemprop="discussionUrl" href="/blog/view/{{{ $post->ID }}}/{{{ $post->post_name }}}#comments">{{ $post->commentsCount }} {{ \Illuminate\Support\Pluralizer::plural('Comment', $post->commentsCount) }}</a>
                </p>
                <!-- ./ post footer -->
                <hr />
            </div>
        @endforeach
    </div>
</div>
<div id="pagination-wrapper" class="row text-center">
    @include('site.flayout.pagination-short', ['results' => $posts])
</div>
<br/>
@stop

@section('nothing')
<?php echo $posts->render(); ?>
@stop
