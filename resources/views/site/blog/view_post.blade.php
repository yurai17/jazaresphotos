@extends('site.' . config('app.layout') . '.default')

{{-- Web site Title --}}
@section('title')
{{{ $post->post_title }}} ::
@parent
@stop

{{-- Update the Meta Title --}}
@section('meta_title')
@parent

@stop

@section('meta_author')
<meta itemprop="author" name="author" content="{{{ $post->author->user_nicename }}}" />
@stop

{{-- Content --}}
@section('content')
<br/>
<div class="row">
    <div class="large-10 small-centered columns" itemid="http://techcrunch.com/2015/03/08/apple-watch-event-live-blog" itemscope itemtype="http://schema.org/BlogPosting">
        <h2 class="text-center"><strong itemprop="headline">{{ $post->post_title }}</strong></h2>
                
        @if(!empty($thumbnail))
            <img itemprop="image" class="thumbnail radius" src="{{ $thumbnail }}" />
        @endif
        
        <div itemprop="articleBody">
            {!! nl2br($post->post_content) !!}
        </div>
        
        <span class="label round" itemprop="datePublished">Posted {{{ $post->post_date }}}</span>
        
        <hr />
        <a id="comments"></a>
        <h5><span itemprop="commentCount">{{ $comment_count }}</span> {{ \Illuminate\Support\Pluralizer::plural('Comment', $comment_count) }}</h5>

        @if ($comment_count > 0)
            @foreach ($comments as $comment)
            <div class="media-object stack-for-small" itemprop="comment" typeOf="Comment">
                <div class="media-object-section">
                    <div class="thumbnail">
                        <img itemprop="image" src="http://placehold.it/80x80" width="80" height="80" alt="Profile Picture">
                    </div>
                </div>
                <div class="media-object-section">
                    <div class="callout radius blog-auth"><span class="muted" itemprop="author">{{{ $comment->comment_author }}}</span>&nbsp;
                    &bull;&nbsp;<span itemprop="dateCreated">{{{ $comment->comment_date }}}</span></div>
                    
                    <hr />
                    
                    <div itemprop="text">
                        {!! nl2br($comment->comment_content) !!}
                    </div>
                </div>
            </div>
            <hr/>
            @endforeach
        @endif
    </div>
</div>
<br/>
@stop
