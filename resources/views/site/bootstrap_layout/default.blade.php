<!DOCTYPE html>
<html lang="en">
    @include('site/layouts/header')
    <body>
        <!-- To make sticky footer need to wrap in a div -->
        <div id="wrap">
            <!-- Navbar -->
            @include('site/layouts/navigation')
            <!-- ./ navbar -->
            
            <!-- Container -->
            <div class="container-fluid">
                <!-- Notifications -->
                </br>
                @include('site/layouts/notifications')
                <!-- ./ notifications -->

                <!-- Content -->
                @yield('content')
                <!-- ./ content -->
            </div>
            <!-- ./ container -->

            <!-- the following div is needed to make a sticky footer -->
            <div id="push"></div>
        </div>
        <!-- ./wrap -->

        @include('site/layouts/footer')
        <!--pre>{{-- var_dump(Session::all()) --}}</pre-->

        <!-- Javascripts
        ================================================== -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        @yield('scripts')
    </body>
</html>
