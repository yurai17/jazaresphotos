<div class="navbar navbar-inverse">
     <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">JPA Photography</a>
        </div>
        
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li {{ (Request::is('/') ? ' class=active' : '') }}><a href="{{{ URL::to('') }}}">Home</a></li>
                @if (\JPAPhotography\Utils\SessionUtil::isAdmin())
                <li class="{{ (Request::is('blog') ? 'active' : '') }} dropdown">
                    <a href="#" class="dropdown-toggle"
                         data-toggle="dropdown" role="button"
                         aria-haspopup="true" aria-expanded="false">Blog<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{{ URL::to('blog') }}}">View All</a></li>
                        <li><a href="{{{ URL::to('blog/create') }}}">Create New</a></li>
                    </ul>
                </li>
                @else
                <li class="{{ (Request::is('blog') ? 'active' : '') }}"><a href="{{{ URL::to('blog') }}}">Blog</a></li>
                @endif
                <li {{ (Request::is('about') ? ' class=active' : '') }}><a href="{{{ URL::to('about') }}}">About</a></li>
                <li {{ (Request::is('about/contact-us') ? ' class=active' : '') }}><a href="{{{ URL::to('about/contact-us') }}}">Contact us</a></li>
            </ul>

            <ul class="nav navbar-nav pull-right">
                {{-- !empty(Auth::user() --}}
                @if (\JPAPhotography\Utils\SessionUtil::check()))
                    {{-- \JPAPhotography\Utils\SessionUtil::user()->hasRole('admin') --}}
                    @if (\JPAPhotography\Utils\SessionUtil::isAdmin())
                        <li><a href="{{{ URL::to('admin') }}}">Admin Panel</a></li>
                    @endif
                    <li><a href="{{{ URL::to('user') }}}">{{{ \JPAPhotography\Utils\SessionUtil::user()['name'] }}}</a></li>
                    <li><a href="{{{ URL::to('auth/logout') }}}">Logout</a></li>
                @else
                    {{--<li {{ (Request::is('auth/login') ? ' class="active"' : '') }}><a href="{{{ URL::to('auth/login') }}}">Login</a></li>
                    <li {{ (Request::is('auth/register') ? ' class="active"' : '') }}><a href="{{{ URL::to('auth/register') }}}">{{{ Lang::get('site.sign_up') }}}</a></li>--}}
                @endif
            </ul>
            <!-- ./ nav-collapse -->
        </div>
    </div>
</div>