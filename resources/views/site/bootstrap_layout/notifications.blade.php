@if (count($errors->all()) > 0)
<div class="callout alert" data-closable>
	<h4>Error!</h4>
	<p>Please check the form below for errors</p>
	<ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>&times;</button>
</div>
@endif
@if (Session::isStarted())
    @if ($message = Session::get('success'))
    <div data-alert class="row alert-box success">
    	<a href="#" class="close">&times;</a>
    	<strong>Success!</strong> 
        @if(is_array($message))
            @foreach ($message as $m)
                {{ $m }}
            @endforeach
        @else
            {{ $message }}
        @endif
    </div>
    @endif
    
    @if ($message = Session::get('error'))
    <div data-alert class="row alert-box alert">
    	<a href="#" class="close">&times;</a>
    	<strong>Error!</strong>
        @if(is_array($message))
        @foreach ($message as $m)
        {{ $m }}
        @endforeach
        @else
        {{ $message }}
        @endif
    </div>
    @endif
    
    @if ($message = Session::get('warning'))
    <div data-alert class="row alert-box warning">
    	<a href="#" class="close">&times;</a>
    	<strong>Warning!</strong>
        @if(is_array($message))
        @foreach ($message as $m)
        {{ $m }}
        @endforeach
        @else
        {{ $message }}
        @endif
    </div>
    @endif
    
    @if ($message = Session::get('info'))
    <div data-alert class="row alert-box info">
    	<a href="#" class="close">&times;</a>
    	<strong>Info!</strong>
        @if(is_array($message))
        @foreach ($message as $m)
        {{ $m }}
        @endforeach
        @else
        {{ $message }}
        @endif
    </div>
    @endif
@endif
