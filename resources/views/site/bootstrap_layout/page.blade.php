@if($results->lastPage() > 1)
    <ul class="pagination" aria-label="Pagination">
        @if($results->currentPage() === 1)
            <li class="disabled" aria-disabled="true">
                <span>&laquo;</span>
            </li>
        @else
            <li class="arrow">
                <a href="{{ $results->previousPageUrl() }}">&laquo;</a>
            </li>
        @endif
        
        @for($i = 1; $i <= $results->lastPage(); $i++)
            @if($i === $results->currentPage())
                <li class="current"><span>{{ $i }}</span></li>
            @else
                <li><a href="{{ $results->url($i) }}">{{ $i }}</a></li>
            @endif
        @endfor

        @if($results->currentPage() === $results->lastPage())
            <li class="arrow disabled" aria-disabled="true">
                <span>&raquo;</span>
            </li>
        @else
            <li class="arrow">
                <a href="{{ $results->nextPageUrl() }}">&raquo;</a>
            </li>
        @endif
    </ul>
@endif