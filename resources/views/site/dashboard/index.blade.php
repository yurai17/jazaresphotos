@extends('site.dblayout.default')

@section('title')
Dashboard :: @parent
@stop

@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
@stop

@section('inline_scripts')
@parent
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.name = null;
    $scope.email = null;
    $scope.message = null;
});
@stop

@section('content')
<div>
    <h2 class="text-center"><strong>Dashboard</strong></h2>
    <div>
        <h3>Newest Users</h3>

        <div>
            <table class="hover stack">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Date Registered</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td><a href="/user/{{ $user->ID }}">View</a> |
                                <a href="/user/{{ $user->ID }}/edit">Edit</a> |
                                <a href="/user/delete/{{ $user->user_login }}">Delete</a>
                            </td>
                            <td>{{ $user->user_login }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at->toFormattedDateString() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <h3>Latest Posts</h3>
        <div>
            <table class="hover stack">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Date Modified</th>
                        <th>Date Published</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td><a href="/blog/view/{{ $post->ID }}/{{ $post->post_name }}">View</a> |
                                <a href="/blog/edit/{{ $post->ID }}">Edit</a> |
                                <a href="/blog/delete/{{ $post->ID }}">Delete</a>
                            </td>
                            <td>{{ $post->post_title }}</td>
                            <td>{{ $post->post_status }}</td>
                            <td>{{ $post->post_modified }}</td>
                            <td>{{ $post->post_date }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
