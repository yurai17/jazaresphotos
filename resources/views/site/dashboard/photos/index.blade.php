@extends('site.dblayout.default')

@section('title')
Photos :: @parent
@stop

@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
@stop

@section('inline_scripts')
@parent
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.name = null;
    $scope.email = null;
    $scope.message = null;
});
@stop

@section('content')
<div>
    <h2 class="text-center"><strong>Photos</strong></h2>
    <div>
        <h3>Users</h3>
        <div>
            <table class="hover stack">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Title</th>
                        <th>URL SQ</th>
                        <th>URL Q</th>
                        <th>URL L</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($images as $image)
                        <tr>
                            <td><a href="/user/{{ $image->flickr_id }}">View</a> |
                                <a href="/user/{{ $image->flickr_id }}/edit">Edit</a> |
                                <a href="/user/delete/{{ $image->flickr_id }}">Delete</a>
                            </td>
                            <td>{{ str_limit($image->title, 20) }}</td>
                            <td>{{ str_limit($image->url_sq, 20) }}</td>
                            <td>{{ str_limit($image->url_q, 20) }}</td>
                            <td>{{ str_limit($image->url_l, 20) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
