@extends('site.dblayout.default')

{{-- Web site Title --}}
@section('title')
Create New Blog ::
@parent
@stop

@section('scripts')
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@stop

@section('inline_scripts')
tinymce.init({
    selector: "textarea",
    theme: "modern",
    height: "600",
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    }
});

var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.title = null;
    $scope.content = null;
    $scope.saveText = function() {
        alert("Saved");
        tinymce.triggerSave();
    }
});

function save() {
    alert('update');
    tinymce.triggerSave();
    return true;
}
@stop

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="large-10 small-centered columns" ng-app="myApp" ng-controller="validateCtrl">
            <h2><strong>Create new blog post</strong></h2>
            <form onsubmit="save()" class="ng-pristine ng-invalid ng-invalid-required" method="POST" action="/blog/create" accept-charset="UTF-8" role="form" name="createForm" novalidate="novalidate">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" placeholder="Title" ng-model="title" value="{{ old('title') }}" required/>
                    <span class="alert label error-span expanded" ng-show="createForm.title.$invalid">
                        <span ng-show="createForm.title.$error.required"><i class="fi-x-circle"></i>&nbsp;Title is required.</span>
                    </span>
                </div>
                
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" placeholder="Content" id="content" rows="10" ng-model="content" required>{{ old('content') }}</textarea>
                    <span class="alert label error-span expanded" ng-show="createForm.content.$invalid">
                        <span ng-show="createForm.content.$error.required"><i class="fi-x-circle"></i>&nbsp;Content is required.</span>
                    </span>
                </div>
                
                <div class="form-group">
                    <input type="submit" class="button" ng-disabled="(createForm.content.$invalid) || (contactForm.title.$invalid)" value="Submit" />
                </div>
            </form>
        </div>
    </div>
@stop
