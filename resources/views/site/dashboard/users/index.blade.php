@extends('site.dblayout.default')

@section('title')
Users :: @parent
@stop

@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
@stop

@section('inline_scripts')
@parent
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.name = null;
    $scope.email = null;
    $scope.message = null;
});
@stop

@section('content')
<div>
    <h2 class="text-center"><strong>Users</strong></h2>
    <div>
        <h3>Users</h3>
        <div>
            <table class="hover stack">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Date Registered</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td><a href="/user/{{ $user->ID }}">View</a> |
                                <a href="/user/{{ $user->ID }}/edit">Edit</a> |
                                <a href="/user/delete/{{ $user->user_login }}">Delete</a>
                            </td>
                            <td>{{ $user->user_login }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at->toFormattedDateString() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
