<!DOCTYPE html>
<html lang="en">
    @include('site/dblayout/header')
    
    <body>
        <!-- Navbar -->
        @include('site/dblayout/navigation')
        <!-- ./ navbar -->

        <!-- To make sticky footer need to wrap in a div -->
        <div id="wrap">
            <!-- Container -->
            <div class="container-fluid">
                <!-- Notifications -->
                @include('site/dblayout/notifications')
                <!-- ./ notifications -->

                <!-- Content -->
                <div class="title-bar hide-for-large">
                    <div class="title-bar-left">
                        <button class="menu-icon" type="button" data-open="offCanvas"></button>
                        <span class="title-bar-title">Foundation</span>
                    </div>
                </div>
                <div class="off-canvas-wrapper">
                    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
                        <!-- Sidebar -->
                        @include('site/dblayout/sidebar')
                        <!-- ./ Sidebar -->
                        <div class="off-canvas-content mainbar" data-off-canvas-content>
                            @yield('content')
                        </div>
                    </div>
                </div>
                <!-- ./ content -->
            </div>
            <!-- ./ container -->

            <!-- the following div is needed to make a sticky footer -->
            <div id="push"></div>
        </div>
        <!-- ./wrap -->

        <!--include('site/dblayout/footer')-->
        <!--pre>{{-- var_dump(Session::all()) --}}</pre-->

        <!-- Javascripts
        ================================================== -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/vendor/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/vendor/what-input.min.js"></script>
        <!--script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/foundation.js"></script-->
        <script src="{{ asset('/js/foundation.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.js"></script>
        @yield('scripts')
        <script>
            $(document).ready(function() {
                $(document).foundation();
            });
            @section('inline_scripts')
                
            @show
        </script>
    </body>
</html>
