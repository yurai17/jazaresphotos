<footer class="footer">
    <div class="row">
        <div class="small-12 medium-6 large-5 columns">
            <p class="logo"><i class="fi-camera"></i> JPA PHOTOS</p> 
            <p class="footer-links">
                <a href="{{{ URL::to('') }}}">Home</a>
                <a href="{{{ URL::to('blog') }}}">Blog</a>
                <a href="{{{ URL::to('about') }}}">About</a>
                <a href="{{{ URL::to('about/contact-us') }}}">Contact</a>
            </p>
            <p class="copywrite">Copyright &copy; {{ date("Y") }}</p>
        </div>
        <div class="small-12 medium-6 large-4 columns vcard">
            <ul class="contact">
                <li><p><i class="fi-marker"></i>1595 Spring Street New Britain, CT 06051</p></li>
                <li><p><i class="fi-telephone"></i>+1-656-453-9966</p></li>
                <li><p><i class="fi-mail"></i>contact@emperor.com</p></li>
            </ul>
        </div>
        <div class="small-12 medium-12 large-3 columns">
            <p class="about">About JPA Photos</p>
            <p class="about subheader">Strike me down, and I will become more powerful than you could possibly imagine.</p>
            <ul class="inline-list social">
                <a target="_blank" href="https://www.facebook.com/jjpaphotography13"><i class="fi-social-facebook"></i></a>
                <a target="_blank" href="https://www.instagram.com/yurai17/"><i class="fi-social-linkedin"></i></a>
            </ul>
        </div>
    </div>
</footer>