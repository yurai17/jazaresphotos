<div id="offCanvas" class="off-canvas position-left reveal-for-large sidebar" aria-hidden="false" data-position="left" data-off-canvas data-is-revealed="true" data-is-sticky="true">
    <div>
        <h4 class="title">Dashboard Menu</h4>
        <ul class="menu vertical" data-drilldown>
            <li><a href="/dashboard"><i class="fi-thumbnails"></i><span>Dashboard</span></a></li>
            <li>
                <a href="/dashboard/users"><i class="fi-torsos-all"></i><span>Users</span></a>
                <ul class="menu vertical{{ (str_contains(Request::url(), 'users') ? ' is-active' : '') }}">
                    <li><a href="/dashboard/users/add"><i class="fi-plus"></i>&nbsp;<span>Add Users</span></a></li>
                    <li><a href="/dashboard/users/edit"><i class="fi-pencil"></i>&nbsp;<span>Edit Users</span></a></li>
                    <li><a href="/dashboard/users/delete"><i class="fi-x"></i>&nbsp;<span>Delete Users</span></a></li>
                </ul>
            </li>
            <li>
                <a href="/dashboard/posts"><i class="fi-page-copy"></i><span>Posts</span></a>
                <ul class="menu vertical{{ (str_contains(Request::url(), 'posts') ? ' is-active' : '') }}">
                    <li><a href="/dashboard/posts/add"><i class="fi-plus"></i>&nbsp;<span>Add Posts</span></a></li>
                    <li><a href="/dashboard/posts/edit"><i class="fi-pencil"></i>&nbsp;<span>Edit Posts</span></a></li>
                    <li><a href="/dashboard/posts/delete"><i class="fi-x"></i>&nbsp;<span>Delete Posts</span></a></li>
                </ul>
            </li>
            <li>
                <a href="/dashboard/photos"><i class="fi-photo"></i><span>Photos</span></a>
                <ul class="menu vertical{{ (str_contains(Request::url(), 'photos') ? ' is-active' : '') }}">
                    <li><a href="/dashboard/photos"><i class="fi-eye"></i>&nbsp;<span>View Photos</span></a></li>
                    <li><a href="/dashboard/photos/add"><i class="fi-refresh"></i>&nbsp;<span>Refresh Photos</span></a></li>
                    <li><a href="/dashboard/photos/edit"><i class="fi-pencil"></i>&nbsp;<span>Edit Photos</span></a></li>
                    <li><a href="/dashboard/photos/delete"><i class="fi-x"></i>&nbsp;<span>Delete Photos</span></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>