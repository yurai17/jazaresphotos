<footer class="footer">
    <div class="row small-up-1 medium-up-2 large-up-2">
        <div class="column">
            <p class="logo text-right"><i class="fi-camera"></i> JPA PHOTOS</p> 
            <ul class="menu">
                <li><a href="{{{ url('') }}}">Home</a><span>&nbsp;&bull;&nbsp;</span></li>
                <li><a href="{{{ url('blog') }}}">Blog</a><span>&nbsp;&bull;&nbsp;</span></li>
                <li><a href="{{{ url('about') }}}">About</a><span>&nbsp;&bull;&nbsp;</span></li>
                <li><a href="{{{ url('about/contact-us') }}}">Contact</a></li>
            </ul>
            <p class="copywrite">Copyright &copy; {{ date("Y") }}</p>
        </div>
        <div class="column">
            <p class="about">About JPA Photos</p>
            <p class="about subheader">Strike me down, and I will become more powerful than you could possibly imagine.</p>
            <ul class="no-bullet inline-list social">
                <li><a target="_blank" href="https://www.facebook.com/jjpaphotography13"><i class="fi-social-facebook"></i></a></li>
                <li><a target="_blank" href="https://www.instagram.com/yurai17/"><i class="fi-social-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</footer>