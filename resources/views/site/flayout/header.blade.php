        
    <head itemscope itemtype="http://schema.org/WebSite">
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8" />
        <title itemprop='name'>
            @section('title')
            JPA Photos
            @show
        </title>
        <link rel="canonical" href="{{ url('/') }}" itemprop="url"/>
    @section('meta_keywords')
        <meta name="keywords" content="photography, photos, photographer, creative" />
    @show

    @section('meta_author')
        <meta name="author" content="Jobert Azares" />
    @show

    @section('meta_description')
        <meta itemprop="about" name="description" content="A site to showcase Jobert Azares' photos" />
    @show
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- CSS
        ================================================== -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/css/foundation.min.css" />
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" />
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />
        <link rel="stylesheet" href="{{ asset('/css/doc.css') }}" />
    @section('css')
    @show
        
        <style>
@section('styles')
@show
        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicons
        ================================================== -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}" />
        <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}" />
        <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}" />
    </head>