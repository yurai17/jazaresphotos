
<div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="large">
            <button class="menu-icon" type="button" data-toggle></button>
            <div class="title-bar-title">Menu</div>
        </div>
        <nav class="top-bar" role="navigation" id="main-menu">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu data-click-open="false" data-closing-time="50">
                    <li{{ (Request::is('/') ? ' class=active' : '') }}><a href="{{{ URL::to('') }}}">Home</a></li>
                    <li{{ (Request::is('gallery') ? ' class=active' : '') }}><a href="{{{ URL::to('gallery') }}}">Gallery</a></li>
                    <li{{ (Request::is('blog') ? ' class=active' : '') }}><a href="{{{ URL::to('blog') }}}">Blog</a></li>
                    <li{{ (Request::is('about') ? ' class=active' : '') }}><a href="{{{ URL::to('about') }}}">About</a></li>
                    <li{{ (Request::is('about/contact-us') ? ' class=active' : '') }}><a href="{{{ URL::to('about/contact-us') }}}">Contact us</a></li>
                </ul>
            </div>
            <div class="top-bar-right">
                <ul class="menu">
                    {{-- !empty(Auth::user() --}}
                    @if (\JPAPhotography\Utils\SessionUtil::check())
                        {{-- \JPAPhotography\Utils\SessionUtil::user()->hasRole('admin') --}}
                        @if (\JPAPhotography\Utils\SessionUtil::isAdmin())
                            <li><a href="{{{ URL::to('dashboard') }}}">Dashboard</a></li>
                        @endif
                        {{--\JPAPhotography\Utils\SessionUtil::user()['name']--}}
                        <li><a href="{{{ URL::to('user') }}}">{{{ \JPAPhotography\Utils\SessionUtil::user()['name'] }}}</a></li>
                        <li><a href="{{{ URL::to('auth/logout') }}}">Logout</a></li>
                    @else
                        <li{{ (Request::is('user/login') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/login') }}}">Login</a></li>
                        <li{{ (Request::is('user/register') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/register') }}}">{{{ Lang::get('site.sign_up') }}}</a></li>
                    @endif
                </ul>
                <!-- ./ nav-collapse -->
            </div>
        </nav>