@foreach(['secondary' => 'Message', 'primary' => 'Message', 'success' => 'Success', 'warning' => 'Warning', 'alert' => 'Alert'] as $msg_type => $title)
    @if(Session::has('alert-' . $msg_type))
        <div class="callout {{ $msg_type }} small" data-closable>
            <h4>{{ $title }}</h4>
            <p>{{ Session::get('alert-' . $msg_type) }}</p>
            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>&times;</button>
        </div>
    @endif
@endforeach

@if (count($errors->all()) > 0)
<div class="callout alert" data-closable>
	<h4>Error!</h4>
	<p>Please check the form below for errors</p>
	<ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>&times;</button>
</div>
@endif
@if (Session::isStarted())
    @if ($message = Session::get('success'))
    <div data-alert class="row alert-box success">
    	<a href="#" class="close">&times;</a>
    	<strong>Success!</strong> 
        @if(is_array($message))
            @foreach ($message as $m)
                {{ $m }}
            @endforeach
        @else
            {{ $message }}
        @endif
    </div>
    @endif
    
    @if ($message = Session::get('error'))
    <div class="callout alert" data-closable>
    	<h4>Error!</h4>
        <p>Please check the form below for errors</p>
        @if(is_array($message))
        @foreach ($message as $m)
        {{ $m }}
        @endforeach
        @else
        {{ $message }}
        @endif
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>&times;</button>
    </div>
    @endif
    
    @if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block fade in">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    	<h4>Warning</h4>
        @if(is_array($message))
        @foreach ($message as $m)
        {{ $m }}
        @endforeach
        @else
        {{ $message }}
        @endif
    </div>
    @endif
    
    @if ($message = Session::get('info'))
    <div class="alert alert-info alert-block fade in">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    	<h4>Info</h4>
        @if(is_array($message))
        @foreach ($message as $m)
        {{ $m }}
        @endforeach
        @else
        {{ $message }}
        @endif
    </div>
    @endif
@endif
