@if($results->lastPage() > 1)
    <ul class="pagination" aria-label="Pagination">
        @if($results->currentPage() === 1)
            <li class="disabled" aria-disabled="true">
                <span title="First Page">&laquo;</span>
            </li>
            <li class="disabled single-arrow" aria-disabled="true">
                <span title="Previous Page">&lsaquo;</span>
            </li>
        @else
            <li class="arrow">
                <a href="{{ $results->url(1) }}" title="First Page">&laquo;</a>
            </li>
            <li class="arrow single-arrow">
                <a href="{{ $results->previousPageUrl() }}" title="Previous Page">&lsaquo;</a>
            </li>
        @endif

        @if($results->currentPage() === $results->lastPage())
            <li class="arrow disabled single-arrow" aria-disabled="true">
                <span title="Next Page">&rsaquo;</span>
            </li>
            <li class="arrow disabled" aria-disabled="true">
                <span title="Last Page">&raquo;</span>
            </li>
        @else
            <li class="arrow single-arrow">
                <a href="{{ $results->nextPageUrl() }}" title="Next Page">&rsaquo;</a>
            </li>
            <li class="arrow">
                <a href="{{ $results->url($results->lastPage()) }}" title="Last Page">&raquo;</a>
            </li>
        @endif
    </ul>
@endif