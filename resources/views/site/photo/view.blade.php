@extends('site.layouts.default')

@section('title')
{{ $post->title }} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-8 col-md-offset-2 text-center">
        <!-- Post Title -->
        <h2><strong>{{ $post->title }}</strong></h2>
        <!-- ./ post title -->

        <!-- Post Content -->
        <a href="{{{ $post['url_l'] }}}" class="img-thumbnail"><img class="img-responsive" src="{{{ $post->url_l }}}" width="{{{ $post->width_l }}}" height="{{{ $post->height_l }}}" alt="{{ $post['title'] }}"/></a>
        <!-- ./ post content -->
    </div>
</div>

<hr />

@stop
