@extends('site.' . config('app.layout') . '.default')

@section('title')
Portfolio :: @parent
@stop

@section('inline_scripts')
$('#carousel-modal').on('open.zf.reveal', function() {
  $('.orbit-container').css({'height': '100em', 'max-height': '100%'});
});
@stop

@section('styles')
.img-wrapper {
    position: relative;
}

.img-wrapper a {
    /*display: block;*/
    position: relative;
    overflow: hidden;
}

.img-wrapper:hover .title {
    opacity: 1;
}

.img-wrapper .title {
    position: absolute;
    overflow: hidden;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: #333;
    opacity: 0;
    padding: 1.5rem;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.4);
}
.item-wrapper:hover .img-wrapper .title {
    bottom: 8px;
}
.pagination li{
  border: 1px solid #cacaca;
  border-radius: 50%;
  padding: 0;
}
.pagination .current, .pagination .disabled  {
    padding: 0;
}
.pagination span {
    padding: 0.1875rem 0.625rem;
    border-radius: 50%;
    display: block;
}
.pagination a {
  border-radius: 50%;
}
@stop

@section('content')
    @if(Request::input('page', 1) == 1)
        <div class="callout primary">
            <div class="row">
            <h2>JPA Photography</h2>
            <p>Sometimes you learn more from your mistakes than you do from your masterpieces. Remember how
               free clouds are. They just lay around in the sky all day long. If you don't like it - change
               it. It's your world. We'll take a little bit of Van Dyke Brown. From all of us here, I want 
               to wish you happy painting and God bless, my friends. There's not a thing in the world wrong
               with washing your brush.</p>
            </div>
        </div>
    @endif
    <div class="gallery">
        <div class="row">
            <ul class="clearing-thumbs row small-up-2 medium-up-3 large-up-4 text-center no-bullet" data-clearing>
                @foreach($posts as $post)
                    <li class="item-wrapper column">
                        <div class="img-wrapper">
                            <a class="thumbnail radius" data-open="carousel-modal" href="#"><!-- {{ $post['url_l'] }} -->
                                <img data-caption="{{ $post['title'] }}"
                                     width="{{ $post['width_q'] }}"
                                     height="{{ $post['height_q'] }}"
                                     src="{{ $post['url_q'] }}" />
                                <div class="title">
                                    <p class="vertical-center">{{ $post['title'] }}</p>
                                </div>
                            </a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div id="pagination-wrapper" class="row text-center">
            @include('site.layouts.page', ['results' => $posts])
        </div>
        
    </div>
    
    <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container">
            <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
            <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
            <li class="is-active orbit-slide">
                <div>
                    <h3 class="text-center">You can also throw some text in here!</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde harum rem, beatae ipsa consectetur quisquam. Rerum ratione, delectus atque tempore sed, suscipit ullam, beatae distinctio cupiditate ipsam eligendi tempora expedita.</p>
                    <h3 class="text-center">This Orbit slide has chill</h3>
                </div>
            </li>
            <li class="orbit-slide">
                <div>
                    <h3 class="text-center">You can also throw some text in here!</h3>
                    <p class="text-center">Follow the lay of the land. It's most important. This is the fun part Let's put a touch more of the magic here.</p>
                    <h3 class="text-center">This Orbit slide has chill</h3>
                </div>
            </li>
            <li class="orbit-slide">
                <div>
                    <h3 class="text-center">You can also throw some text in here!</h3>
                    <p class="text-center">We don't want to set these clouds on fire. All you have to do is let your imagination go wild. Every day I learn. Every highlight needs it's own personal shadow. There comes a nice little fluffer. We can always carry this a step further. There's really no end to this.</p>
                    <h3 class="text-center">This Orbit slide has chill</h3>
                </div>
            </li>
            <li class="orbit-slide">
                <div>
                    <h3 class="text-center">You can also throw some text in here!</h3>
                    <p class="text-center">You have to make almighty decisions when you're the creator. Even trees need a friend. We all need friends. Just think about these things in your mind - then bring them into your world. If you've been in Alaska less than a year you're a Cheechako.</p>
                    <h3 class="text-center">This Orbit slide has chill</h3>
                </div>
            </li>
        </ul>
        <nav class="orbit-bullets">
            <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
            <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
            <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
            <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
        </nav>
    </div>
    
@stop