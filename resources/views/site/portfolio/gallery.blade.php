@extends('site.' . config('app.layout') . '.default')
{{-- Content --}}
@section('content')

@foreach ($posts as $post)
<!-- div class="col-sm-1" -->
    <div class="col-md-3">
	<h4>
		<strong>{{ $post['title'] }}</strong>
	</h4>
	
		<a href="{{{ $post['url_l'] }}}" class="thumbnail"><img src="{{{ $post['url_q'] }}}" width="{{{ $post['width_q'] }}}" height="{{{ $post['height_q'] }}}" alt="{{ $post['title'] }}"></a>
	</div>
<!-- /div -->
@endforeach
<div style="clear:both;"></div>
<?php echo $posts->render(); ?>
@stop
