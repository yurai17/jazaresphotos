@extends('site.' . config('app.layout') . '.default')

@section('title')
Portfolio :: @parent
@stop

@section('added_jquery_script')
$('div.reveal').on('open.zf.reveal', function(event){
    $(this).children('h2').click(function(event){
        $(this).children('button').trigger('click');
    });
    $(this).children('div').click(function(event){
        $(this).children('button').trigger('click');
    });
    $(this).children('div').children('img').click(function(event){
        $(this).children('button').trigger('click');
    });
});
$('a.thumbnail').click(function(event){
    event.preventDefault();
    var modalID = $(this).attr('data-toggle');
    var imgSrc = $(this).attr('href');
    $('#' + modalID + ' img').attr('height', 1024);
    $('#' + modalID + ' img').attr('src', "{{ asset('/images/loading.gif') }}");
    $('#' + modalID + ' img').attr('src', imgSrc);
});
@stop

@section('styles')
div[id^="modal-"] {
    width: 100% !important;
    margin-top: 0;
    top: 0 !important;
}

#loading_gif {
    position: absolute;
    left: 50%;
    top: 50%;
}
.img-wrapper {
    position: relative;
}

.img-wrapper a {
    position: relative;
    overflow: hidden;
}

.thumbnail:hover .title {
    opacity: 1;
}

.img-wrapper .title {
    position: absolute;
    overflow: hidden;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: #333;
    opacity: 0;
    padding: 1.5rem;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.4);
}
.img-wrapper .title em {
    font-size: 0.8rem;
}
.item-wrapper:hover .img-wrapper .title {
    bottom: 8px;
}

.gallery {
  position: relative;
  width: 100%;
  margin-top: -1rem;
  overflow: hidden;
}
  .gallery_background {
    position: absolute;
    top: 0;
    left: -50%;
    width: 200%;
    height: 200%;
    z-index: -1;
    /*-webkit-transform: translate3d(0, 0, 0);
    -webkit-filter: blur(100px); 
    -moz-filter: blur(100px);
    -o-filter: blur(100px); 
    -ms-filter: blur(100px);
    filter: blur(100px);
    filter: url("/svg/filter.svg#blur");
    filter:progid:DXImageTransform.Microsoft.Blur(pixelradius=100);*/
  }
  .gallery_background image{
      width: 150%;
  }
  .hero__title {
    position: relative;
    text-align: center;
    font-size: 5em;
    color: white;
  }

.footer {
    margin-top: -1rem;}
  
@stop

@section('nothing')
.gallery {
  position: relative;
  width: 100%;
  padding: 2em 0;}
#galleryCanvas {
    position: absolute;
    display: block;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -10;}
.blur-ie {
    display: none;
    transform: scale(1.4);}
@stop

@section('content')
    <div class="gallery">
        <br/>
        <div class="row">
            <ul class="clearing-thumbs smal-up-2 medium-up-3 large-up-4 no-bullet">
                @foreach($posts as $post)
                    <li class="item-wrapper column">
                        <div class="img-wrapper text-center" itemid="{{ $post->url_q }}" itemscope itemtype="http://schema.org/Photograph">
                            <div itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                <a itemprop="contentUrl" class="thumbnail radius" data-toggle="modal-{{ $post->flickr_id }}" href="{{ $post->url_l }}">
                                    <img id="mainPhotoImage"
                                            filter="url(#blur)"
                                            alt="{{ $post->title }}"
                                            data-caption="{{ $post->title }}"
                                            width="{{ $post->width_q }}" 
                                            height="{{ $post->height_q }}" 
                                            src="{{ $post->url_q }}" itemprop="thumbnailUrl"/>
                                    <div class="title">
                                        <p class="vertical-center" itemprop="caption"><strong>{{ $post->title }}</strong></p>
                                        <p class="vertical-center" itemprop="dateTaken"><em>{{ $post->date_taken }}</em></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        
        <div id="pagination-wrapper" class="row text-center">
            @include('site.flayout.page', ['results' => $posts])
        </div>
    </div>
    @foreach($posts as $post)
    <div class="full reveal text-center" id="modal-{{ $post->flickr_id }}" data-reveal data-reset-on-close="false" data-close-on-esc="true">
        <h2><strong>{{ $post->title }}</strong></h2>
        <div>
            <img class="thumbnail" height="1024" src="{{ asset('/images/loading.gif') }}" alt="{{ $post->title }}" />
        </div>
        <button class='close-button' data-close aria-label='Close reveal' type='button'>
            <span aria-hidden='true'>&times;</span>
        </button>
    </div>
    @endforeach
@stop