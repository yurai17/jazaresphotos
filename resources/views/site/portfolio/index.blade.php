@extends('site.' . config('app.layout') . '.default')

@section('title')
Portfolio :: @parent
@stop

@section('scripts')
<script src="{{ asset('/js/lightbox.js') }}"></script>
@stop

@section('added_jquery_script')
$('.clearing-thumbs').magnificPopup({
  delegate: 'a', // child items selector, by clicking on it popup will open
  type: 'image',
  gallery: {
      enabled: true,
      preload: [1,2],
  },
  image: {
      titleSrc: 'alt',
      verticalFit: true
  }
});
$('a.thumbnail').click(function(event){
    event.preventDefault();
});
@stop

@section('scripts_are_nothing')
$('#pictureModal').on('open.zf.reveal', function(event){
    //var topPos = ($(window).height() - $('#pictureModal').height()) / 2;
    //$('#pictureModal').css({'max-height':'1024px', 'margin-top':'0', 'top':topPos});
    $('#pictureModal, #pictureModal h2, #pictureModal div, #pictureModal div img').click(function(event){
        //$('#pictureModal button').trigger('click');
        $('#pictureModal').foundation('close');
    });
});
$('a.thumbnail').click(function(event){
    event.preventDefault();
    var title = $(this).children('img').attr('alt');
    var imgSrc = $(this).attr('href');
    $('#pictureModal #modal-pic-title').text(title);
    $('#pictureModal img').attr('height', 1024);
    $('#pictureModal img').attr('alt', title);
    $('#pictureModal img').attr('src', "{{ asset('/images/loading.gif') }}");
    $('#pictureModal img').attr('src', imgSrc);
});
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('/css/lightbox.css') }}" />
@stop

@section('styles')
#pictureModal {
    height: 100%;
    max-height: 100%;
    width: 100% !important;
}
#loading_gif {
    position: absolute;
    left: 50%;
    top: 50%;
}
.img-wrapper {
    position: relative;
}

.img-wrapper a {
    /*display: block;*/
    position: relative;
    overflow: hidden;
}

.thumbnail:hover .title {
    opacity: 1;
}

.img-wrapper .title {
    position: absolute;
    overflow: hidden;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: #333;
    opacity: 0;
    padding: 1.5rem;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.4);
}
.img-wrapper .title em {
    font-size: 0.8rem;
}
.item-wrapper:hover .img-wrapper .title {
    bottom: 8px;
}

.gallery {
  position: relative;
  width: 100%;
  overflow: hidden;
}
  .gallery_background {
    position: absolute;
    top: 0;
    left: -50%;
    width: 200%;
    height: 200%;
    z-index: -1;
    /*-webkit-transform: translate3d(0, 0, 0);
    -webkit-filter: blur(100px); 
    -moz-filter: blur(100px);
    -o-filter: blur(100px); 
    -ms-filter: blur(100px);
    filter: blur(100px);
    filter: url("/svg/filter.svg#blur");
    filter:progid:DXImageTransform.Microsoft.Blur(pixelradius=100);*/
  }
  .gallery_background image{
      width: 150%;
  }
  .hero__title {
    position: relative;
    text-align: center;
    font-size: 5em;
    color: white;
  }

.footer {
    margin-top: -1rem;}
  
@stop

@section('nothing')
.gallery {
  position: relative;
  width: 100%;
  padding: 2em 0;}
#galleryCanvas {
    position: absolute;
    display: block;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -10;}
.blur-ie {
    display: none;
    transform: scale(1.4);}
@stop

@section('content')
    <div class="gallery">
        <br/>
        <div class="row">
            <ul class="clearing-thumbs smal-up-2 medium-up-3 large-up-4 no-bullet" data-clearing>
                @foreach($posts as $post)
                    <li class="item-wrapper column">
                        <div class="img-wrapper text-center" itemid="{{ $post->url_q }}" itemscope itemtype="http://schema.org/Photograph">
                            <div itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                <a itemprop="contentUrl" class="thumbnail radius" href="{{ $post->url_l }}" alt="{{ $post->title }}">
                                    <img id="mainPhotoImage"
                                            alt="{{ $post->title }}"
                                            width="{{ $post->width_q }}" 
                                            height="{{ $post->height_q }}" 
                                            src="{{ $post->url_q }}"
                                            itemprop="thumbnailUrl"/>
                                    <div class="title">
                                        <p class="vertical-center" itemprop="caption"><strong>{{ $post->title }}</strong></p>
                                        <p class="vertical-center" itemprop="dateTaken"><em>{{ $post->date_taken }}</em></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        
        <div id="pagination-wrapper" class="row text-center">
            @include('site.flayout.page', ['results' => $posts])
        </div>
        <br/>
    </div>
    <div class="full reveal text-center" id="pictureModal" data-reveal data-reset-on-close="true" data-close-on-esc="true">
        <h2><strong id="modal-pic-title">Picture</strong></h2>
        <div>
            <img class="thumbnail" id="modalPic" height="1024" src="{{ asset('/images/loading.gif') }}" alt="Picture" />
        </div>
        <button class='close-button' data-close aria-label='Close reveal' type='button'>
            <span aria-hidden='true'>&times;</span>
        </button>
    </div>
@stop