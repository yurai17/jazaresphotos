@extends('site.' . config('app.layout') . '.default')

@section('title')
Portfolio :: @parent
@stop

@section('content')
    @if($posts->currentPage() == 1)
        <div class="callout primary">
            <div class="row">
            <h2>JPA Photography</h2>
            <p>Sometimes you learn more from your mistakes than you do from your masterpieces. Remember how
               free clouds are. They just lay around in the sky all day long. If you don't like it - change
               it. It's your world. We'll take a little bit of Van Dyke Brown. From all of us here, I want 
               to wish you happy painting and God bless, my friends. There's not a thing in the world wrong
               with washing your brush.</p>
            </div>
        </div>
    @endif

    <div class="gallery">
        <pre>
        {!! print_r($posts) !!}
        </pre>
    </div>