@extends('site.' . config('app.layout') . '.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.settings') }}} ::
@parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
@parent
body {
    background: #f2f2f2;
}
@stop

@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
@stop

@section('inline_scripts')
var app = angular.module('myApp', []);
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.controller('validateCtrl', function($scope) {
    $scope.user_login = '{{{ Input::old('username', $user->user_login) }}}';
    $scope.email = '{{{ Input::old('email', $user->email) }}}';
    $scope.password = null;
    $scope.password_confirm = null;
});
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="medium-10 small-centered columns">
        <h2 class="text-center"><strong>Edit your settings</strong></h3>
        
        <div class="callout alert text-center">
            <strong>Reminder!</strong> All form items are required and cannot be empty.
        </div>
        
        <form class="form-horizontal" method="post" action="{{ URL::to('user/' . $user->ID . '/edit') }}" name="userEditForm" autocomplete="off" ng-app="myApp" ng-controller="validateCtrl" novalidate>
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <!-- ./ csrf token -->
            <!-- General tab -->
            <div class="tab-pane active" id="tab-general">
                <!-- username -->
                <div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
                    <label for="username">Username</label>
                    <input ng-model="user_login" class="form-control" type="text" name="user_login" id="username" value="{{{ Input::old('username', $user->user_login) }}}" required/>
                    <span class="label label-danger" ng-show="userEditForm.user_login.$invalid">
                        <span ng-show="userEditForm.user_login.$error.required">Username is required.</span>
                        {{ $errors->first('user_login', '<span ng-show="userEditForm.user_login.$error.required">:message</span>') }}
                    </span>
                </div>
                <!-- ./ username -->

                <!-- Email -->
                <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
                    <label for="email">Email</label>
                    <input ng-model="email" class="form-control" type="email" name="email" id="email" value="{{{ Input::old('email', $user->email) }}}" required/>
                    <span class="label label-danger" ng-show="userEditForm.email.$invalid">
                        <span ng-show="userEditForm.email.$error.required">Email is required.</span>
                        <span ng-show="userEditForm.email.$error.email">Invalid email address.</span>
                        {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                    </span>
                </div>
                <!-- ./ email -->

                <!-- Password -->
                <div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
                    <label for="password">Password</label>
                    <input ng-model="password" class="form-control" type="password" name="password" id="password" value="" required/>
                    <span class="label label-danger" ng-show="userEditForm.password.$invalid">
                        <span ng-show="userEditForm.password.$error.required">Password is required.</span>
                        {{ $errors->first('password', '<span class="help-inline">:message</span>') }}
                    </span>
                </div>
                <!-- ./ password -->

                <!-- Password Confirm -->
                <div class="form-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
                    <label for="password_confirmation">Password Confirm</label>
                    <input ng-model="password_confirm" class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" required/>
                    <span class="label label-danger" ng-show="userEditForm.password_confirm.$invalid || (password_confirm !== password)">
                        <span ng-show="userEditForm.password_confirm.$error.required || (password_confirm !== password)">Password and confirmation must match.</span>
                        {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
                    </span>
                </div>
                <!-- ./ password confirm -->
            </div>
            <!-- ./ general tab -->

            <!-- Form Actions -->
            <div class="form-group">
                <button type="submit" class="button">Update</button>
            </div>
            <!-- ./ form actions -->
        </form>
    </div>
</div>
@stop
