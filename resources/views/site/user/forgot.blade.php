@extends('site.' . config('app.layout') . '.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.forgot_password') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="medium-6 small-centered columns">
        <form action="/user/forgot" method="post" >
            {{ csrf_field() }}
            <fieldset class="fieldset">
                <legend><h3><strong>Reset Password</strong></h3></legend>
                <div class="form-group">
                    <label for="email">E-mail Address</label>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="E-mail Address" />
                </div>
                <div class="form-group">
                    <input type="submit" class="button" value="Send Reset Email" />
                </div>
            </fieldset>
        </form>
    </div>
</div>
@stop
