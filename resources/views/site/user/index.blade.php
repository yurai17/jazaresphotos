@extends('site.' . config('app.layout') . '.default')

{{-- Web site Title --}}
@section('title')
{{{ $user->name }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="medium-10 small-centered columns">
        <h2 class="text-center"><strong>My Profile - {{ $user->name }}</strong></h3>
        <dl>
            <dt>Username</dt>
            <dd>{{ $user->user_login }}</dd>
            <dt>Name</dt>
            <dd>{{ $user->name }}</dd>
            <dt>Email</dt>
            <dd>{{ $user->email }}</dd>
            <dt>Date Registered</dt>
            <dd>{{ $user->created_at->toFormattedDateString() }}</dd>
        </dl>
    </div>
</div>
@stop
