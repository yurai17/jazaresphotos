@extends('site.' . config('app.layout') . '.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="medium-6 columns">
        <form action="/user/login" method="post" novalidate>
            {{ csrf_field() }}
            <fieldset class="fieldset">
                <legend><h3><strong>Login</strong></h3></legend>
                <div class="form-group">
                    <label for="username">Username or Email</label>
                    <input type="text" name="login" value="{{ old('login') }}" placeholder="Your username or email" required/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" required/>
                </div>
                <div class="form-group">
                    <input type="submit" class="button" value="Log in" />
                </div>
                <a href="/user/forgot">Forgot Password?</a>
            </fieldset>
        </form>
    </div>
    <div class="medium-6 columns">
        <form action="/user/register" method="post">
            {{ csrf_field() }}
            <fieldset class="fieldset">
                <legend><h3><strong>Register</strong></h3></legend>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" value="{{ old('username') }}" placeholder="Your username" required/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="Your email" required/>
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{ old('name') }}" placeholder="Your complete name" required/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" required/>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" name="password_confirmation" required/>
                </div>
                <div class="form-group">
                    <input type="submit" class="button" value="Register" />
                </div>
            </fieldset>
        </form>
    </div>
</div>
{{-- Confide::makeLoginForm()->render() --}}
@stop
